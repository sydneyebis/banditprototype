﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class digging : MonoBehaviour {

	public bool canDig = true;
	public int health = 10;
	public int currentHealth;
	// Use this for initialization
	void Start () {
		currentHealth = health;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentHealth <= 0) {
			Destroy (this.gameObject);
		}
	}
	public void getDug(int amount){
		currentHealth -= amount;

	}
}
