﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    float timer = 0;
	bool climb;
	public float gravityScale = 9.8f;
	//DirtHealth dirtHealth;
	GameObject climbingSpace;
    // Use this for initialization
    void Start() {
		climb = false;
		Debug.Log (climb);
		climbingSpace = GameObject.FindGameObjectWithTag("underGround");

    }

    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime;
		Physics.gravity = new Vector3(0, -gravityScale, 0);
		var z = Input.GetAxis("Vertical") * Time.deltaTime * -3.0f;
		if (climb == true) {
			var y = Input.GetAxis("Horizontal") * Time.deltaTime * 10.0f;
			transform.Translate (0, y, 0);
		}
		if (climb == true) {
			//this.GetComponent<Rigidbody>().;
			//this.GetComponent<Rigidbody>().isKinematic = false;
			gravityScale = 5f;
		}
		if (climb == false) {
			//this.GetComponent<Rigidbody>().useGravity = true;
			//this.GetComponent<Rigidbody>().isKinematic = true;
			gravityScale = 9.8f;
		}
        transform.Translate(0, 0, z);
    }
	public void OnTriggerStay(Collider other) { 
		//		other.GetComponent<tag>;
		//Debug.Log("I am in a collider");
        if (other.gameObject.tag == "diggable") { 
			var y = Input.GetAxis("Horizontal") * Time.deltaTime * 5.0f;
			var z = Input.GetAxis("Vertical") * Time.deltaTime * -3.0f;
            GameObject soonToBeHole = other.gameObject;
			if ((y < 0) || (z > 0) && timer >= 1) {
				//if ( && (climb == true)) {
					//soonToBeHole.GetComponent<digging> ().getDug (3);
					//dirtHealth.getDug (5);
					//timer = 0;
				//} else { 
					soonToBeHole.GetComponent<digging> ().getDug (3);
					//dirtHealth.getDug (5);
					timer = 0;
				}
			}  


  
		if(other.gameObject == climbingSpace){
			//Debug.Log ("We are under the earth!");
			climb = true;
		}
	}
	void OnTriggerExit(Collider other){
		if (other.gameObject == climbingSpace) {
			

			climb = false;
		}
	}
}
